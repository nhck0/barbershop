'use strict';

global.$ = {
	path: {
		task: require('./gulp/paths/tasks.js')
	},
	gulp: require('gulp'),
	spritesmith: require('gulp.spritesmith'),
	del: require('del'),
	browserSync: require('browser-sync').create(),
	gp: require('gulp-load-plugins')(),
    cssunit: require('gulp-css-unit')
};

$.path.task.forEach(function(taskPath){
	require(taskPath)();
});

$.gulp.task('default', $.gulp.series(
	'clean',
	'sprite',
	'fonts',
	'svgSpriteBuild',
	$.gulp.parallel(
		'sass',
		'pug'
	),
	'bower',
	'useref',
	$.gulp.parallel(
		'watch',
		'serve'
	)
));
