'use strict';

module.exports = function() {
	$.gulp.task('sprite', function() {
    return $.gulp.src('./src/assets/sprite/*.png')
   		.pipe($.spritesmith({
        imgName: 'sprite.png',
        imgPath: './src/assets/sprite.png',
        cssName: 'sprite.scss'
    }))

    .pipe($.gp.if('*.png', $.gulp.dest('./src/assets/')))
    .pipe($.gp.if('*.scss', $.gulp.dest('./src/styles/_misc')))

    	.on('end', function () {
			$.gulp.src(['src/assets/**/*.*', '!src/assets/sprite/**/*.*'])
        	.pipe($.gulp.dest('build/assets/'))
    	})
	});
};