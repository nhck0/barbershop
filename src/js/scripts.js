var link = document.querySelector('.login-link');
var popupSign = document.querySelector('.modal-login');
var popupClose = document.querySelector('.modal-close');
var popupLogin = popupSign.querySelector("[name=login]");
var popupPassword = popupSign.querySelector("[name=password]");
var popupForm = popupSign.querySelector("form");
var storageLogin = localStorage.getItem("login");

var isStorageSupport = true;

try{
    storageLogin = localStorage.getItem("login");
} catch (err){
    isStorageSupport = false;
}

link.addEventListener("click", function (evt) {
    evt.preventDefault();
    popupSign.classList.add("modal-show");
    if(storageLogin){
        popupLogin.value = storageLogin;
        popupPassword.focus();
    } else{
        popupLogin.focus();
    }
});

popupClose.addEventListener("click", function (evt) {
    evt.preventDefault();
    popupSign.classList.remove("modal-show");
    popupSign.classList.remove("modal-error");
});

popupForm.addEventListener("submit", function(evt){
    if(!popupLogin.value || !popupPassword.value){
        evt.preventDefault();
        popupSign.classList.remove("modal-error");
        popupSign.offsetWidth = popupSign.offsetWidth;
        popupSign.classList.add("modal-error");

    } else{
        if(isStorageSupport){
            localStorage.setItem("login", popupLogin.value);
        }
    }
});


window.addEventListener('keydown', function (evt) {
   if(evt.keyCode === 27){
       evt.preventDefault();
       if(popupSign.classList.contains("modal-show")){
           popupSign.classList.remove("modal-show");
           popupSign.classList.remove("modal-error");
       }
   }
});